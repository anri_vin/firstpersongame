using System;
using System.Collections.Generic;
using System.Text;
using OpenTK;

namespace FirstPersonGL
{
	class Player
	{
		private float LEVEL_SIZE_X;
		private float LEVEL_SIZE_Z;
		private Vector3d m_position = new Vector3d ();
		private double m_facing = 0;
		private double m_pitch = 0;

		public Vector3d Position {
			get { return m_position; }
			set { 
				if (value.X > LEVEL_SIZE_X - 10)
					value.X = LEVEL_SIZE_X - 10;
				if (value.X < -LEVEL_SIZE_X + 10)
					value.X = -LEVEL_SIZE_X + 10;
				if (value.Z > LEVEL_SIZE_Z - 10)
					value.Z = LEVEL_SIZE_Z - 10;
				if (value.Z < -LEVEL_SIZE_Z + 10)
					value.Z = -LEVEL_SIZE_Z + 10;
				m_position = value;
			}
		}

		public double Facing {
			get { return m_facing; }
			set { m_facing = Utils.WrapAngle (value); }
		}

		public double Pitch {
			get { return m_pitch; }
			set { m_pitch = Utils.LimitAngle (value, -89, 89); }
		}

		public Vector3d Forwards {
			get {
				float r = MathHelper.DegreesToRadians ((float)Facing);
				return new Vector3d (-Math.Sin (r), 0, -Math.Cos (r));
			}
		}

		public Vector3d Sideways {
			get {
				float r = MathHelper.DegreesToRadians ((float)Facing);
				return new Vector3d (-Math.Cos (r), 0, Math.Sin (r));
			}
		}

		public Player (float level_size_x, float level_size_z)
		{
			LEVEL_SIZE_X = level_size_x;
			LEVEL_SIZE_Z = level_size_z;
		}
	}
}
