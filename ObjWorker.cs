﻿using System;
using System.IO;
using OpenTK;

namespace FirstPersonGL
{
	public static class ObjWorker
	{
		public static ObjFile ReadFile (string file)
		{
			ObjFile objFile = new ObjFile ();
			using (var reader = new StreamReader (file)) {
				while (reader.Peek () >= 0) {
					float x, y, z;
					var currentLine = reader.ReadLine ();
					//If it is comment;
					if (currentLine.StartsWith ("#"))
						continue;
					if (currentLine.StartsWith ("v ")) {
						currentLine = currentLine.Remove (0, 2);
						var vertices = currentLine.Split (new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
						if (vertices.Length != 3)
							throw new FormatException ("Input file contains vertex without 3 coordinates.");
						x = Single.Parse (vertices [0]);
						y = Single.Parse (vertices [1]);
						z = Single.Parse (vertices [2]);
						objFile.V.Add (new Vector3 (x, y, z));
					} else if (currentLine.StartsWith ("vt ")) {
						currentLine = currentLine.Remove (0, 3);
						var textures = currentLine.Split (new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
						if (textures.Length != 2)
							throw new FormatException ("Input file contains texture without 2 coordinates.");
						x = Single.Parse (textures [0]);
						textures [1] = textures [1].Split (new [] { ' ' }, 2) [0];
						y = Single.Parse (textures [1]);
						objFile.VT.Add (new Vector2 (x, y));
					} else if (currentLine.StartsWith ("vn ")) {
						currentLine = currentLine.Remove (0, 3);
						var verticesNormals = currentLine.Split (new[] { ' ' }, 3, StringSplitOptions.RemoveEmptyEntries);
						if (verticesNormals.Length != 3)
							throw new FormatException ("Input file contains vertex normal without 3 coordinates.");
						x = Single.Parse (verticesNormals [0]);
						y = Single.Parse (verticesNormals [1]);
						z = Single.Parse (verticesNormals [2]);
						objFile.VN.Add (new Vector3 (x, y, z));

					} else if (currentLine.StartsWith ("f ")) {
						currentLine = currentLine.Remove (0, 2);
						var faces = currentLine.Split (new[] { ' ' }, 4);
						if (faces.Length < 3)
							throw new FormatException ("Input file contains face without 3 points.");
						if (faces.Length == 3)
							objFile.PrimitiveType = OpenTK.Graphics.OpenGL.PrimitiveType.Triangles;
						if (faces.Length == 4)
							objFile.PrimitiveType = OpenTK.Graphics.OpenGL.PrimitiveType.Quads;

						var face = new Faces ();
						int i = 0;
						foreach (var f in faces) {
							var components = f.Split (new [] { '/' }, 3, StringSplitOptions.None);

							if (!String.IsNullOrWhiteSpace (components [0]))
								face.V [i] = objFile.V [Int32.Parse (components [0]) - 1];
							if (!String.IsNullOrWhiteSpace (components [1]))
								face.VT [i] = objFile.VT [Int32.Parse (components [1]) - 1];
//							if (!String.IsNullOrWhiteSpace (components [2]))
							//	face.VN [i] = objFile.VN [Int32.Parse (components [1]) - 1];
							i++;
						}
						objFile.F.Add (face);
					}
				}
			}
			return objFile;
		}

	}
}