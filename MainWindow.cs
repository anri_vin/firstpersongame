using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace FirstPersonGL
{
	class MainWindow : GameWindow
	{
		private const float LEVEL_SIZE_X = 2000;
		private const float LEVEL_SIZE_Z = 500;

		private const float REPEAT_FLOOT_TEXTURE = 32.0f;
		private const float REPEAT_WALL_TEXTURE_WIDTH = 8.0f;
		private const float REPEAT_WALL_TEXTURE_HEIGHT = 1.0f;

		private Player P = new Player (LEVEL_SIZE_X, LEVEL_SIZE_Z);

		private Vector2 lastMousePos = new Vector2 ();

		private int AsphaltTextureID = 0;
		private int GrassTextureID = 0;
		private int HouseWallTextureID = 0;
		private int CurbTextureID = 0;
		private int WoodTextureID = 0;
		private int LeavesTextureID = 0;
		private int MetallTextureID = 0;
		//private int BallTextureID = 0;

		private float VerticalSpeed = -1.0f, HorizontalSpeed = 1.0f;
		private const float FallingConst = 0.1f;
		private Vector3 ballPosition = new Vector3 (100, 100, 0);

		private ObjFile ballFile;

		protected override void OnLoad (EventArgs e)
		{
			// Setup OpenGL capabilities
			GL.Enable (EnableCap.DepthTest);
			GL.Enable (EnableCap.CullFace);
			GL.Enable (EnableCap.ColorMaterial);
			GL.Enable (EnableCap.Lighting);
			GL.Enable (EnableCap.Light0);

			// Setup background colour
			GL.ClearColor (Color.LightSkyBlue);

			// Center the mouse before we start
			CenterMouse ();
			System.Windows.Forms.Cursor.Hide ();

			// Load textures
			GrassTextureID = Utils.UploadTexture ("./Textures/Grass2.jpg");
			AsphaltTextureID = Utils.UploadTexture ("./Textures/Asphalt.jpg");
			HouseWallTextureID = Utils.UploadTexture ("./Textures/House.jpg");
			CurbTextureID = Utils.UploadTexture ("./Textures/Curb.jpg");
			WoodTextureID = Utils.UploadTexture ("./Textures/Wood.jpg");
			LeavesTextureID = Utils.UploadTexture ("./Textures/Leaves.jpg");
			MetallTextureID = Utils.UploadTexture ("./Textures/Metall.jpg");
			//BallTextureID = Utils.UploadTexture ("./Textures/Ball.jpg");
			// Enable 2D textures

			ballFile = ObjWorker.ReadFile ("./Models/Ball.obj");
			GL.Enable (EnableCap.Texture2D);

			lastMousePos = new Vector2 (Mouse.X, Mouse.Y);
		}

		public MainWindow () : base (1024, 768, new GraphicsMode (32, 24, 0, 4))
		{
		}

			
		//
		// OnResize
		//  - Game window sizing and 3D projection setup
		//
		protected override void OnResize (EventArgs e)
		{
			int w = Width;
			int h = Height;
			float aspect = 1;

			// Calculate aspect ratio, checking for divide by zero
			if (h > 0) {
				aspect = (float)w / (float)h;
			}

			// Initialise the projection view matrix
			GL.MatrixMode (MatrixMode.Projection);
			GL.LoadIdentity ();

			// Setup a perspective view
			float FOVradians = MathHelper.DegreesToRadians (45);
			Matrix4 perspective = Matrix4.CreatePerspectiveFieldOfView (FOVradians, aspect, 1, 4000);
			GL.MultMatrix (ref perspective);

			// Set the viewport to the whole window
			GL.Viewport (0, 0, w, h);
		}

		//
		// OnRenderFrame
		//  - Draw a single 3D frame
		//
		protected override void OnRenderFrame (FrameEventArgs e)
		{
			// Clear the screen
			GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			// Initialise the model view matrix
			GL.MatrixMode (MatrixMode.Modelview);
			GL.LoadIdentity ();

			// Draw the scene
			DrawScene ();

			// Display the new frame
			SwapBuffers ();
		}


		//
		// OnUpdateFrame
		//  - Updates the game world every frame
		//
		protected override void OnUpdateFrame (FrameEventArgs e)
		{
			#region Keyboard control handle
			if (Keyboard [Key.Escape])
				Exit ();

			if (Keyboard [Key.W]) {
				P.Position += P.Forwards *
				(Keyboard [Key.LShift] || Keyboard [Key.RShift] 
						? 3 
						: Keyboard [Key.LControl] ? 1 : 2)
				* 100 * e.Time;

			}

			if (Keyboard [Key.S]) {
				P.Position -= P.Forwards * 100 * e.Time;
			}

			if (Keyboard [Key.A]) {
				P.Position += P.Sideways * 100 * e.Time;
			}

			if (Keyboard [Key.D]) {
				P.Position -= P.Sideways * 100 * e.Time;
			}


			P.Position = (Keyboard [Key.ControlLeft] || Keyboard [Key.ControlRight]) 
				? new Vector3d (P.Position.X, -20, P.Position.Z) : new Vector3d (P.Position.X, 0, P.Position.Z);
			#endregion

			#region Mouse control handle
			if (Focused) {
				Vector2 delta = lastMousePos - new Vector2 (OpenTK.Input.Mouse.GetState ().X, OpenTK.Input.Mouse.GetState ().Y);
				lastMousePos += delta;

				P.Facing += delta.X * 0.5;
				P.Pitch += delta.Y * 0.5;
				Console.WriteLine ("Width: {0}; Height: {1}", (delta.X), (delta.Y));
				CenterMouse ();
			}
			recalcBallPosition ();
	
			#endregion
		}

		private void CenterMouse ()
		{
			OpenTK.Input.Mouse.SetPosition (Bounds.Left + Bounds.Width / 2, Bounds.Top + Bounds.Height / 2);
			lastMousePos = new Vector2 (OpenTK.Input.Mouse.GetState ().X, OpenTK.Input.Mouse.GetState ().Y);
		}


		private void DrawScene ()
		{
			// Shift the camera to the player's position and orientation in the world
			GL.Rotate (-P.Pitch, 1, 0, 0);
			GL.Rotate (-P.Facing, 0, 1, 0);
			GL.Translate (-P.Position);

			//Setup light position
			GL.Light (LightName.Light0, LightParameter.Position, new Vector4 (0.0f, 10.0f, 0.0f, 0.0f));

			// Draw the world
			DrawObj (ballFile, ballPosition.X, ballPosition.Y, ballPosition.Z, 0);
			DrawGrass (0, -43, 0, 0);
			DrawGrass (0, -43, 0, 180);
			DrawGrass (2000, -43, 0, 0);
			DrawGrass (2000, -43, 0, 180);
			DrawGrass (-2000, -43, 0, 0);
			DrawGrass (-2000, -43, 0, 180);

			DrawGrass (2000, -43, -400, 0);
			DrawGrass (2000, -43, 400, 180);
			DrawGrass (-2000, -43, -400, 0);
			DrawGrass (-2000, -43, 400, 180);

			DrawHouse (0, -43, 0, 0);
			DrawWall (0, -43, 0, 0);
			for (int i = -1870; i < 2000; i += 500) {
				DrawSidewalk (400.0f, i, -42.9f, -LEVEL_SIZE_Z + 400, 90);
				DrawTree (i + 200, -43, -LEVEL_SIZE_Z + 200, 0);
				DrawTree (i + 300, -43, -LEVEL_SIZE_Z + 100, 0);
				DrawTree (i + 350, -43, -LEVEL_SIZE_Z + 300, 0);
			}
			DrawSidewalk (8000.0f, -4000, -49, 0, 0);
			DrawSidewalk (8000.0f, -4000, -49, -100, 0);
			DrawCurb (8000.0f, -4000, -49, -100, 0);
			DrawCurb (8000.0f, 4000, -49, 100, 180);
		}

		void recalcBallPosition ()
		{
			ballPosition.X += HorizontalSpeed;
			if (ballPosition.Y > -43) {
				VerticalSpeed += FallingConst;
			} else {
				VerticalSpeed *= -0.5f;
			}
			ballPosition.Y -= VerticalSpeed;

		}

		private void DrawGrass (float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);
			GL.BindTexture (TextureTarget.Texture2D, GrassTextureID);
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapS, Convert.ToInt32 (TextureWrapMode.Repeat));
			GL.TexParameter (TextureTarget.Texture2D, TextureParameterName.TextureWrapT, Convert.ToInt32 (TextureWrapMode.Repeat));

			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.GhostWhite);
			GL.Color3 (Color.White);

			GL.Normal3 (0.0f, 1.0f, 0.0f);
			GL.TexCoord2 (0.0f, LEVEL_SIZE_X * 2 / 100);
			GL.Vertex3 (-LEVEL_SIZE_X, 0, -LEVEL_SIZE_Z);

			GL.TexCoord2 (LEVEL_SIZE_Z * 2 / 100, LEVEL_SIZE_X * 2 / 100);
			GL.Vertex3 (-LEVEL_SIZE_X, 0, -100);

			GL.TexCoord2 (LEVEL_SIZE_Z * 2 / 100, 0.0f);
			GL.Vertex3 (LEVEL_SIZE_X, 0, -100);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (LEVEL_SIZE_X, 0, -LEVEL_SIZE_Z);

			GL.End ();
			GL.PopMatrix ();

		}


		private void DrawObj (ObjFile file, float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			//GL.BindTexture (TextureTarget.Texture2D, BallTextureID);
			GL.Scale (new Vector3 (0.05f, 0.05f, 0.05f));
			GL.Begin (file.PrimitiveType);
			foreach (var face in file.F) {
				GL.Color3 (Color.White);
				GL.Normal3 (new Vector3 (0.0f, 1.0f, 0.0f));
				GL.Material (MaterialFace.FrontAndBack, MaterialParameter.Diffuse, Color.White);
				GL.TexCoord2 (face.VT [0]);
				GL.Vertex3 (face.V [0]);
				GL.TexCoord2 (face.VT [1]);
				GL.Vertex3 (face.V [1]);
				GL.TexCoord2 (face.VT [2]);
				GL.Vertex3 (face.V [2]);
				if (file.PrimitiveType == PrimitiveType.Quads) {
					GL.TexCoord2 (face.VT [3]);
					GL.Normal3 (face.VN [3]);
					GL.Vertex3 (face.V [3]);
				}
			}

			GL.End ();
			GL.PopMatrix ();
		}

		private void DrawHouse (float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, HouseWallTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.White);

			//Front wall
			GL.Normal3 (0.0f, 0.5f, 0.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (LEVEL_SIZE_X, 500.0f, -LEVEL_SIZE_Z);

			GL.TexCoord2 (REPEAT_WALL_TEXTURE_WIDTH, 0.0f);
			GL.Vertex3 (-LEVEL_SIZE_X, 500.0f, -LEVEL_SIZE_Z);

			GL.TexCoord2 (REPEAT_WALL_TEXTURE_WIDTH, REPEAT_WALL_TEXTURE_HEIGHT);
			GL.Vertex3 (-LEVEL_SIZE_X, 0, -LEVEL_SIZE_Z);

			GL.TexCoord2 (0.0f, REPEAT_WALL_TEXTURE_HEIGHT);
			GL.Vertex3 (LEVEL_SIZE_X, 0, -LEVEL_SIZE_Z);

			GL.End ();
			GL.PopMatrix ();
		}

		private void DrawWall (float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, MetallTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.White);

			//Front wall
			GL.Normal3 (0.0f, 0.5f, 0.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (-LEVEL_SIZE_X, 100.0f, LEVEL_SIZE_Z);

			GL.TexCoord2 (40.0f, 0.0f);
			GL.Vertex3 (LEVEL_SIZE_X, 100.0f, LEVEL_SIZE_Z);

			GL.TexCoord2 (40.0f, REPEAT_WALL_TEXTURE_HEIGHT);
			GL.Vertex3 (LEVEL_SIZE_X, 0, LEVEL_SIZE_Z);

			GL.TexCoord2 (0.0f, REPEAT_WALL_TEXTURE_HEIGHT);
			GL.Vertex3 (-LEVEL_SIZE_X, 0, LEVEL_SIZE_Z);

			GL.End ();
			GL.PopMatrix ();
		}


		private void DrawSidewalk (float length, float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, AsphaltTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.White);

			GL.Normal3 (0.0f, 0.5f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, 100.0f);

			GL.TexCoord2 (length / 50, 0.0f);
			GL.Vertex3 (length, 0.0f, 100.0f);

			GL.TexCoord2 (length / 50, 2.0f);
			GL.Vertex3 (length, 0.0f, 0.0f);

			GL.TexCoord2 (0.0f, 2.0f);
			GL.Vertex3 (0.0f, 0.0f, 0.0f);

			GL.End ();
			GL.PopMatrix ();
		}

		private void DrawCurb (float length, float x, float y, float z, float ori)
		{
			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, CurbTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.White);

			GL.Normal3 (0.0f, 0.5f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, 4.0f);

			GL.TexCoord2 (length / 50, 0.0f);
			GL.Vertex3 (length, 0.0f, 4.0f);

			GL.TexCoord2 (length / 50, 1.0f);
			GL.Vertex3 (length, 6.0f, 4.0f);

			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (0.0f, 6.0f, 4.0f);

			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (0.0f, 6.0f, 4.0f);

			GL.TexCoord2 (length / 50, 1.0f);
			GL.Vertex3 (length, 6.0f, 4.0f);

			GL.TexCoord2 (length / 50, 0.0f);
			GL.Vertex3 (length, 6.0f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (0.0f, 6.0f, 0.0f);

			GL.End ();
			GL.PopMatrix ();
		}

		private void DrawTree (float x, float y, float z, float ori)
		{
			float height = 200, width = 10;

			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, WoodTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.BurlyWood);

			GL.Normal3 (0.0f, 0.5f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, width);
			GL.TexCoord2 (0.0f, 10.0f);
			GL.Vertex3 (0.0f, height, width);
			GL.TexCoord2 (1.0f, 10.0f);
			GL.Vertex3 (0.0f, height, 0.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, 0.0f);
			GL.TexCoord2 (0.0f, 10.0f);
			GL.Vertex3 (0.0f, height, 0.0f);
			GL.TexCoord2 (1.0f, 10.0f);
			GL.Vertex3 (width, height, 0.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width, 0.0f, 0.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width, 0.0f, 0.0f);
			GL.TexCoord2 (0.0f, 10.0f);
			GL.Vertex3 (width, height, 0.0f);
			GL.TexCoord2 (1.0f, 10.0f);
			GL.Vertex3 (width, height, width);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width, 0.0f, width);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width, 0.0f, width);
			GL.TexCoord2 (0.0f, 10.0f);
			GL.Vertex3 (width, height, width);
			GL.TexCoord2 (1.0f, 10.0f);
			GL.Vertex3 (0.0f, height, width);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (0.0f, 0.0f, width);
			GL.End ();
			GL.PopMatrix ();

			//Crown

			GL.PushMatrix ();

			GL.Translate (x, y, z);
			GL.Rotate (ori, 0, 1, 0);

			GL.BindTexture (TextureTarget.Texture2D, LeavesTextureID);
			GL.Begin (PrimitiveType.Quads);
			GL.Material (MaterialFace.Front, MaterialParameter.Diffuse, Color.White);
			GL.Color3 (Color.BurlyWood);

			GL.Normal3 (0.0f, 0.5f, 0.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (-25.0f, height - 100, width + 25);
			GL.TexCoord2 (0.0f, 2.0f);
			GL.Vertex3 (-25.0f, height + 25, width + 25);
			GL.TexCoord2 (1.0f, 2.0f);
			GL.Vertex3 (-25.0f, height + 25, -25.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-25.0f, height - 100, -25.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (-25.0f, height - 100, -25.0f);
			GL.TexCoord2 (0.0f, 2.0f);
			GL.Vertex3 (-25.0f, height + 25, -25.0f);
			GL.TexCoord2 (1.0f, 2.0f);
			GL.Vertex3 (width + 25, height + 25, -25.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width + 25, height - 100, -25.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 25, height - 100, -25.0f);
			GL.TexCoord2 (0.0f, 2.0f);
			GL.Vertex3 (width + 25, height + 25, -25.0f);
			GL.TexCoord2 (1.0f, 2.0f);
			GL.Vertex3 (width + 25, height + 25, width + 25);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width + 25, height - 100, width + 25);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 25, height - 100, width + 25);
			GL.TexCoord2 (0.0f, 2.0f);
			GL.Vertex3 (width + 25, height + 25, width + 25);
			GL.TexCoord2 (1.0f, 2.0f);
			GL.Vertex3 (-25.0f, height + 25, width + 25);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-25.0f, height - 100, width + 25);

			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-25.0f, height - 100, width + 25);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (-25.0f, height - 100, -25.0f);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (width + 25, height - 100, -25.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 25, height - 100, width + 25);

			//Second crown cube
			GL.Vertex3 (-5.0f, height - 70, width + 40);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (-5.0f, height - 10, width + 40);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (-5.0f, height - 10, -5.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-5.0f, height - 70, -5.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (-5.0f, height - 70, -5.0f);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (-5.0f, height - 10, -5.0f);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (width + 40, height - 10, -5.0f);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width + 40, height - 70, -5.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 40, height - 70, -5.0f);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (width + 40, height - 10, -5.0f);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (width + 40, height - 10, width + 40);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (width + 40, height - 70, width + 40);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 40, height - 70, width + 40);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (width + 40, height - 10, width + 40);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (-5.0f, height - 10, width + 40);
			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-5.0f, height - 70, width + 40);

			GL.TexCoord2 (1.0f, 0.0f);
			GL.Vertex3 (-5.0f, height - 70, width + 40);
			GL.TexCoord2 (1.0f, 1.0f);
			GL.Vertex3 (-5.0f, height - 70, -5.0f);
			GL.TexCoord2 (0.0f, 1.0f);
			GL.Vertex3 (width + 40, height - 70, -5.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width + 40, height - 70, width + 40);

			//Third crown cube
			GL.Vertex3 (-40.0f, height - 65, width - 10);
			GL.TexCoord2 (0.0f, 0.5f);
			GL.Vertex3 (-40.0f, height - 20, width - 10);
			GL.TexCoord2 (0.5f, 0.5f);
			GL.Vertex3 (-40.0f, height - 20, -40.0f);
			GL.TexCoord2 (0.5f, 0.0f);
			GL.Vertex3 (-40.0f, height - 65, -40.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (-40.0f, height - 65, -40.0f);
			GL.TexCoord2 (0.0f, 0.5f);
			GL.Vertex3 (-40.0f, height - 20, -40.0f);
			GL.TexCoord2 (0.5f, 0.5f);
			GL.Vertex3 (width - 10, height - 20, -40.0f);
			GL.TexCoord2 (0.5f, 0.0f);
			GL.Vertex3 (width - 10, height - 65, -40.0f);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width - 10, height - 65, -40.0f);
			GL.TexCoord2 (0.0f, 0.5f);
			GL.Vertex3 (width - 10, height - 20, -40.0f);
			GL.TexCoord2 (0.5f, 0.5f);
			GL.Vertex3 (width - 10, height - 20, width - 10);
			GL.TexCoord2 (0.5f, 0.0f);
			GL.Vertex3 (width - 10, height - 65, width - 10);

			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width - 10, height - 65, width - 10);
			GL.TexCoord2 (0.0f, 0.5f);
			GL.Vertex3 (width - 10, height - 20, width - 10);
			GL.TexCoord2 (0.5f, 0.5f);
			GL.Vertex3 (-40.0f, height - 20, width - 10);
			GL.TexCoord2 (0.5f, 0.0f);
			GL.Vertex3 (-40.0f, height - 65, width - 10);

			GL.TexCoord2 (0.5f, 0.0f);
			GL.Vertex3 (-40.0f, height - 65, width - 10);
			GL.TexCoord2 (0.5f, 0.5f);
			GL.Vertex3 (-40.0f, height - 65, -40.0f);
			GL.TexCoord2 (0.0f, 0.5f);
			GL.Vertex3 (width - 10, height - 65, -40.0f);
			GL.TexCoord2 (0.0f, 0.0f);
			GL.Vertex3 (width - 10, height - 65, width - 10);

			GL.End ();
			GL.PopMatrix ();
		}
	}
}
