﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace FirstPersonGL
{
	public class ObjFile
	{
		//List of vertices
		public List<Vector3> V = new List<Vector3> ();
		//List of texture coordinates
		public List<Vector2> VT = new List<Vector2> ();
		//List of normals
		public List<Vector3> VN = new List<Vector3> ();
		//List of faces
		public List<Faces> F = new List<Faces> ();

		public PrimitiveType PrimitiveType;
	}

	public class Faces
	{
		public Vector3[] V = new Vector3[4];
		public Vector2[] VT = new Vector2[4];
		public Vector3[] VN = new Vector3[4];
	}
}